import { Injectable, Optional, Inject } from "@nestjs/common";
import { CommandExecutor } from "./CommandExecutor";
import { ModuleRef } from "@nestjs/core";
import { CommandContextConstructor } from "./CommandContext";

@Injectable()
export class CommandService {
	static readonly ROOT_CONTEXT = Symbol("COMMAND_SERVICE_ROOT_CONTEXT");
	/// Modulo
	constructor(
		private readonly moduleRef: ModuleRef,
		@Inject(CommandService.ROOT_CONTEXT)
		@Optional()
		private readonly rootContextClass: CommandContextConstructor
	) {}
	/**
	 * Create a new command executor
	 */
	async createExecutor(): Promise<CommandExecutor> {
		const executor = new CommandExecutor(this);
		await executor.pushContext(this.rootContextClass);
		return executor;
	}
	/// Get a service to use on the context
	getService<T>(service: any): T {
		return this.moduleRef.get<T>(service, { strict: false });
	}
}
