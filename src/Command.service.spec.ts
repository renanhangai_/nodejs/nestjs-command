import { Test, TestingModule } from "@nestjs/testing";
import sinon from "sinon";
import { CommandModule } from "./Command.module";
import { CommandService } from "./Command.service";
import { CommandExecutor } from "./CommandExecutor";
import { CommandContext } from "./CommandContext";
import { Injectable } from "@nestjs/common";

// Root test command context class
class TestRootCommandContext extends CommandContext {}

describe("CommandService", () => {
	let commandService: CommandService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [
				CommandModule.forRoot({
					rootContextClass: TestRootCommandContext,
				}),
			],
		}).compile();
		commandService = app.get<CommandService>(CommandService);
	});

	// Test root
	it("should be a CommandService", () => {
		expect(commandService).toBeInstanceOf(CommandService);
	});

	it("should get other services", async () => {
		// Other service to test
		@Injectable()
		class TestService {}

		// Get the other service
		const app: TestingModule = await Test.createTestingModule({
			imports: [
				CommandModule.forRoot({
					rootContextClass: TestRootCommandContext,
				}),
			],
			providers: [TestService],
		}).compile();
		const service = app.get<CommandService>(CommandService);

		expect(service.getService<TestService>(TestService)).toBeInstanceOf(TestService);
	});

	// Test the executor
	describe("CommandExecutor", () => {
		let executor: CommandExecutor;
		beforeEach(async () => {
			executor = await commandService.createExecutor();
		});
		afterEach(async () => {
			if (executor) {
				await executor.close();
				executor = null;
			}
		});
		it("should have CommandExecutor", async () => {
			expect(executor).toBeInstanceOf(CommandExecutor);
			expect(executor.getContext()).toBeInstanceOf(TestRootCommandContext);
		});

		it("should push new contexts", async () => {
			class TestCommandContext extends CommandContext {}
			await executor.pushContext(TestCommandContext);
			expect(executor.getContext()).toBeInstanceOf(TestCommandContext);
		});

		it("should run a command", async () => {
			const execSpy = sinon.spy();
			class TestCommandContext extends CommandContext {
				exec = execSpy;
			}
			await executor.pushContext(TestCommandContext);
			await executor.exec("test     command   arg");
			expect(execSpy.getCall(0).args[0]).toStrictEqual(["test", "command", "arg"]);

			// Noop
			await executor.exec("");
		});

		it("should quit the command", async () => {
			const rootContext = executor.getContext();
			class TestCommandContext extends CommandContext {}

			await executor.pushContext(TestCommandContext);
			await executor.exec("quit");
			expect(executor.getContext()).toBe(rootContext);

			await executor.pushContext(TestCommandContext);
			await executor.quit();
			expect(executor.getContext()).toBe(rootContext);

			const quitNormalStub = sinon.fake();
			executor.once("quit", quitNormalStub);
			await executor.exec("quit");
			expect(quitNormalStub.calledOnce).toBe(true);

			const quitAgainStub = sinon.fake();
			executor.once("quit", quitAgainStub);
			await executor.exec("anything");
			expect(quitNormalStub.calledOnce).toBe(true);

			expect(executor.getContext()).toBe(null);
		});
	});
});
