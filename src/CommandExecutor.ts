import { EventEmitter } from "events";
import { CommandService } from "./Command.service";
import stringArgv from "string-argv";
import { CommandContext } from "./CommandContext";

export class CommandExecutor extends EventEmitter {
	private contextStack: CommandContext[] = [];
	/**
	 *
	 * @param commandService
	 */
	constructor(public readonly commandService: CommandService) {
		super();
	}
	/**
	 * Roda um comando
	 * @param command
	 */
	async exec(command: string): Promise<any> {
		// Se já saiu do executor
		if (this.contextStack.length <= 0) {
			this.emit("quit");
			return;
		}

		// Roda o argumento
		let result: any;
		const args = stringArgv(command);
		if (args.length <= 0) return null;
		if (args[0] === "quit") {
			await this.quit();
		} else {
			const context = this.contextStack[this.contextStack.length - 1];
			result = await context.exec(args);
		}
		if (this.contextStack.length <= 0) this.emit("quit");
		return result;
	}
	/// Fecha o executor
	async close() {
		while (this.contextStack.length > 0) {
			const context = this.contextStack.pop();
			await context.close();
		}
		this.emit("quit");
	}
	/// Sai da stack atual
	async quit() {
		if (this.contextStack.length > 0) {
			const context = this.contextStack.pop();
			await context.close();
		}
	}
	/// Pusha um novo contexto
	async pushContext<T extends CommandContext>(contextClass: {
		new (executor: CommandExecutor): T;
	}): Promise<T> {
		const context = new contextClass(this);
		await context.setup();
		this.contextStack.push(context);
		return context;
	}
	/// Pega o contexto atual
	getContext(): any {
		if (this.contextStack.length <= 0) return null;
		return this.contextStack[this.contextStack.length - 1];
	}
}
