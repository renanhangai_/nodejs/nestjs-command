import { Module, DynamicModule } from "@nestjs/common";
import { CommandService } from "./Command.service";
import { CommandContextConstructor } from "./CommandContext";

export type CommandModuleOptions = {
	rootContextClass: CommandContextConstructor;
};

@Module({
	providers: [CommandService],
	exports: [CommandService],
})
export class CommandModule {
	static forRoot(options: CommandModuleOptions): DynamicModule {
		const rootContextProvider = {
			provide: CommandService.ROOT_CONTEXT,
			useValue: options.rootContextClass,
		};
		return {
			module: CommandModule,
			providers: [rootContextProvider],
		};
	}
}
