describe("@renanhangai/nestjs-command", () => {
	it("should export modules", () => {
		const { CommandModule } = require("./");
		expect(CommandModule).toBeTruthy();
	});
	it("should export service", () => {
		const { CommandService } = require("./");
		expect(CommandService).toBeTruthy();
	});
	it("should export context", () => {
		const { CommandContext } = require("./");
		expect(CommandContext).toBeTruthy();
	});
});
