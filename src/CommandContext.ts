import { CommandExecutor } from "./CommandExecutor";

/// Command context base type
export type CommandContextConstructor = { new (executor: CommandExecutor): CommandContext };

/**
 * Base para o contexto de um comando
 */
export abstract class CommandContext {
	constructor(protected readonly executor: CommandExecutor) {}
	/// Inicia o comando
	setup(): any {}
	/// Fecha o comando
	close(): any {}
	/// Executa um comando
	exec(args: string[]): any {}
}
